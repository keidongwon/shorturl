#pragma once

class generator
{
public:
	static string base62_encode(long long value);
	static long long base62_decode(string value);
};
