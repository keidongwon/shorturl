#include "stdafx.h"
#include "generator.h"

const char base62[] = { "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" };
const int BASE = 62;

int strpos(const char *s, int c)
{
	char *p = strchr((char*)s, c);
	return p ? (p - s) : -1;
}

string generator::base62_encode(long long value)
{
	char result[50] = { 0, };
	int idx = 0;
	do {
		int mod = value % BASE;
		result[idx] = base62[mod];
		idx++;
	} while ((value = value / BASE));
	result[idx] = '\0';
	return string(result);
}

long long generator::base62_decode(string value)
{
	long long result = 0L;
	long long mul = 1L;
	int pos = 0;

	for (auto c : value) {
		pos = strpos(base62, c);
		result += pos * mul;
		mul *= BASE;
	}
	return result;
}
