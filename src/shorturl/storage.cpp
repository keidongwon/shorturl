#include "stdafx.h"
#include "storage.h"

storage* storage::instance = nullptr;

storage::storage()
{
	index = 1000000000;
}

storage::~storage()
{
	map_url2short.clear();
	map_short2url.clear();
}

storage* storage::get() {
	if (instance == nullptr)
		instance = new storage();
	return instance;
}

void storage::free() {
	if (instance != nullptr)
	{
		delete instance;
		instance = nullptr;
	}
}

long long storage::next() {
	return ++index;
}
