#include "stdafx.h"
#include "server.h"
#include <cpprest/http_listener.h>
#include "bolt/boost_log_wrapper.h"
#include "bolt/json_config.h"
#include "handler.h"

using namespace web::http::experimental::listener;

server::server()
{
	address_ = L"127.0.0.1";
	port_ = 80;
}

void server::init()
{
	init_boost_log();

	cbolt::json_config config;
	if (config.load(L"config.json")) {
		address_ = L"http://" + config.get_string(L"address");
		port_ = config.get_int(L"port");
	}
	BOOST_LOG_TRIVIAL(info) << "shorturl server - " << address_;
}

void server::run()
{
	try
	{
		http_listener listener(address_);
		listener.support(methods::GET, handler::handle_get);
		listener.support(methods::POST, handler::handle_post);
		//listener.support(methods::OPTIONS, handler::handle_options);
		listener
			.open()
			.then([&listener]() { BOOST_LOG_TRIVIAL(info) << "starting to listen..."; })
			.wait();

		while (true) {
			this_thread::sleep_for(chrono::milliseconds(1000));
		}
	}
	catch (std::exception const & e)
	{
		BOOST_LOG_TRIVIAL(error) << e.what() << endl;
		exit(1);
	}
}
