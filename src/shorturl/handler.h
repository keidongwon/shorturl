#pragma once
#include <cpprest/http_listener.h>

using namespace web::http;

class handler
{
public:
	handler() {}
	virtual ~handler() {}
	static void handle_get(http_request request);
	static void handle_post(http_request request);
	static void handle_options(http_request request);

	static void url_form(http_request request);
	static void url_generate(http_request request);
	static void redirect(http_request request);

	static void not_found(http_request request);
};
