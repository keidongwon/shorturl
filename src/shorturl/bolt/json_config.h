﻿#pragma once
#pragma warning(disable:4996)
#include <string>
#include "lib/rapidjson/document.h"

/*
주의 :
json_config 에서 사용할 config file 은 유니코드 형식 (UTF-8, UTF-16 등)으로 저장되어야 한다.
ASCII 형식을 사용할 경우, 영문자, 숫자 외의 문자 (한글 등)가 들어갈 경우 crash 될 수 있다.
*/

using namespace std;
using namespace rapidjson;

typedef GenericDocument<UTF16<> > WDocument;

namespace cbolt {

class json_config
{
public:
    json_config();
    virtual ~json_config();
    WDocument& operator()() { return *document; }

    bool load(const wchar_t *path);
    int get_int(const wchar_t *key);
    bool get_bool(const wchar_t *key);
    wstring get_string(const wchar_t *key);

private:
    WDocument *document;
    char *buffer;
};

}
