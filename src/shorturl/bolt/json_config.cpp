﻿#include "json_config.h"
#include "lib/rapidjson/istreamwrapper.h"
#include "lib/rapidjson/prettywriter.h"
#include "lib/rapidjson/filereadstream.h"

namespace cbolt {

json_config::json_config()
{
    buffer = NULL;
    document = NULL;
}

json_config::~json_config()
{
    if (buffer)	delete[] buffer;
    if (document) delete document;
}

bool json_config::load(const wchar_t *path)
{
    FILE *fp = _wfopen(path, L"rb");
	if (!fp) return false;
    fseek(fp, 0, SEEK_END);
    long filesize = ftell(fp);
    fseek(fp, 0, 0);
    if (filesize == 0) return false;
    buffer = new char[filesize + 1];
    FileReadStream bis(fp, buffer, filesize);
    AutoUTFInputStream<unsigned, FileReadStream> eis(bis);
    document = new WDocument();
    document->ParseStream<0, AutoUTF<unsigned> >(eis);
    fclose(fp);

    return true;
}

int json_config::get_int(const wchar_t *key)
{
    return document->HasMember(key) ? (*document)[key].GetInt() : -1; 
}

bool json_config::get_bool(const wchar_t *key) 
{ 
    return document->HasMember(key) ? (*document)[key].GetBool() : false;
}

wstring json_config::get_string(const wchar_t *key)
{
    return document->HasMember(key) ? wstring((*document)[key].GetString()) : L"";
}

} // namespace cbolt
