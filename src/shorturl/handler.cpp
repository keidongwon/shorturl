#include "stdafx.h"
#include "handler.h"
#include <iostream>
#include <locale>
#include <codecvt>
#include "cpprest/filestream.h"
#include "generator.h"
#include "storage.h"
#include "bolt/boost_log_wrapper.h"
#include "bolt/strutil.h"

using namespace Concurrency::streams;

void handler::handle_options(http_request request)
{
	http_response response(status_codes::OK);
	response.headers().add(U("Allow"), U("GET, POST, OPTIONS"));
	response.headers().add(U("Access-Control-Allow-Origin"), U("*"));
	response.headers().add(U("Access-Control-Allow-Methods"), U("GET, POST, OPTIONS"));
	response.headers().add(U("Access-Control-Allow-Headers"), U("Content-Type"));
	request.reply(response);
}

void handler::handle_get(http_request request)
{
	auto path = request.relative_uri().path();
	if (path == L"/") {
		url_form(request);
	}
	else if (path == L"/generate" || path == L"/generate/") {
		url_generate(request);
	}
	else if (path.length() == 7) {
		redirect(request);
	}
	else {
		not_found(request);
	}
}

void handler::handle_post(http_request request)
{
	auto path = request.relative_uri().path();
	if (path == L"/")
		request.reply(status_codes::OK, U("shorturl generate service"));
	else
		not_found(request);
}

void handler::url_form(http_request request)
{
	std::wifstream wif(L"static/index.html");
	wif.imbue(std::locale(std::locale::empty(), new std::codecvt_utf8<wchar_t>));
	std::wstringstream wss;
	wss << wif.rdbuf();
	
	http_response response(status_codes::OK);
	response.headers().add(U("Content-Type"), U("text/html; charset=utf-8"));
	response.set_body(wss.str());

	request.reply(response);
}

void handler::url_generate(http_request request)
{
	auto http_get_vars = uri::split_query(request.request_uri().query());
	wstring url = http_get_vars[L"t_url"];

	if (url.length() == 0) {
		http_response response(200);
		response.headers().add(L"refresh", L"0; URL=http://localhost");
		request.reply(response);
		return;
	}

	string shorturl = "";

	auto result = storage::get()->map_url2short.find(url);
	if (result != storage::get()->map_url2short.end()) {
		storage::get()->map_url_count[url]++;
		shorturl = result->second;
		BOOST_LOG_TRIVIAL(info) << "short url found : " << result->second;
	}
	else {
		shorturl = generator::base62_encode(storage::get()->next());
		storage::get()->map_url_count[url]++;
		storage::get()->map_url2short[url] = shorturl;
		storage::get()->map_short2url[shorturl] = url;
		BOOST_LOG_TRIVIAL(info) << "short url generated";
		BOOST_LOG_TRIVIAL(info) << L"Real URL : " << uri::decode(url);
		BOOST_LOG_TRIVIAL(info) << "Short URL : " << shorturl;
	}

	std::wifstream wif(L"static/result.html");
	wif.imbue(std::locale(std::locale::empty(), new std::codecvt_utf8<wchar_t>));
	std::wstringstream wss;
	wss << wif.rdbuf();
	wstring body = wss.str();
	wstring target = L"http://localhost/" + cbolt::strutil::mbs_to_wcs(shorturl);
	body = cbolt::strutil::replaceall(body, L"{%URL%}", uri::decode(url));
	body = cbolt::strutil::replaceall(body, L"{%SHORT_URL%}", target);

	http_response response(status_codes::OK);
	response.headers().add(U("Content-Type"), U("text/html; charset=utf-8"));
	response.set_body(body);

	request.reply(response);
}

void handler::redirect(http_request request)
{
	auto path = request.relative_uri().path();
	string shorturl = cbolt::strutil::wcs_to_mbs(path);
	shorturl.erase(0, 1);
	auto result = storage::get()->map_short2url.find(shorturl);
	if (result != storage::get()->map_short2url.end()) {
		wstring newpath = L"0; URL=" + uri::decode(result->second);
		http_response response(200);
		response.headers().add(L"refresh", newpath);
		request.reply(response);
	}
	else {
		not_found(request);
	}
}

void handler::not_found(http_request request)
{
	concurrency::streams::fstream::open_istream(U("static/404.html"), std::ios::in).then([=](concurrency::streams::istream is)
	{
		request.reply(status_codes::NotFound, is, U("text/html"))
			.then([](pplx::task<void> t)
		{
			try {
				t.get();
			}
			catch (...) {
			}
		});
	}).then([=](pplx::task<void>t)
	{
		try {
			t.get();
		}
		catch (...) {

			request.reply(status_codes::InternalError, U("INTERNAL ERROR "));
		}
	});
}
