#pragma once

class server
{
public:
	server();
	virtual ~server() {}

	void init();
	void run();

private:
	wstring address_;
	int port_;
};
