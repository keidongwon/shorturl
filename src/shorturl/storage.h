#pragma once
#include <map>

class storage
{
public:
	storage();
	~storage();
	static storage* get();
	static void free();
	long long next();

public:
	long long index;
	map<wstring, string> map_url2short;
	map<string, wstring> map_short2url;
	map<wstring, long> map_url_count;

private:
	static storage* instance;
};
